package info.deskchan.sapi_sharp;

import info.deskchan.core.Plugin;
import info.deskchan.core.PluginProxyInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;

public class Main implements Plugin {

    private static Main instance;
    private Path SApiSharpBridgePath = null;
    private static String SApiSharpBridgeFileName =  "DeskChanSapiBridgeNet4.exe";
    private Process SApiSharpBridgeProcess;
    private Thread SApiSharpBridgeOutputThread;

    private static PluginProxyInterface pluginProxy;

    @Override
    public boolean initialize(PluginProxyInterface ppi){
        instance = this;
        pluginProxy = ppi;

        SApiSharpBridgePath = pluginProxy.getPluginDirPath().resolve(SApiSharpBridgeFileName).toFile().toPath();

        Process SApiSharpBridgeProcess;
        ProcessBuilder b = new ProcessBuilder(SApiSharpBridgePath.toString());
        try {
            SApiSharpBridgeProcess = b.start();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        Runnable r = () -> {
            String line;
            BufferedReader input = new BufferedReader(new InputStreamReader(SApiSharpBridgeProcess.getInputStream()));
            try {
                while ((line = input.readLine()) != null) {
                    pluginProxy.log("SApiSharpBridgeProcess:" + line);
                }
            } catch (IOException e) {
                //TODO
                e.printStackTrace();
            }
        };
        SApiSharpBridgeOutputThread = new Thread(r);
        SApiSharpBridgeOutputThread.start();

        pluginProxy.log("SApi sharp module is ready");
        return true;
    }

    @Override
    public void unload(){

    }


}